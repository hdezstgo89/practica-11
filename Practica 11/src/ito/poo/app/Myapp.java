package ito.poo.app;

import ito.poo.Excepciones.ExcepcionValidaFechaRegreso;
import ito.poo.clases.Aplicacion;

public class Myapp {
	public static void main(String[] args) throws ExcepcionValidaFechaRegreso {
		try {
		Aplicacion.run();
		}catch (ExcepcionValidaFechaRegreso e) {
			System.err.println(e.getMessage());
		}	
	}
}
