package ito.poo.clases;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import ito.dsc.input.FormInput;
import ito.dsc.output.FormOutput;
import ito.poo.Excepciones.ExcepcionValidaFechaRegreso;

public class Aplicacion implements Serializable {

	private static final long serialVersionUID = 1L;
	static private ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();
	static private ArrayList<Viajes> viajes = new ArrayList<Viajes>();
	static private FormInput menu = new FormInput();
	static private final int FIN = 7;
	/*******************************************************************************/
	static void generaMenu() {
		menu.addItemMenu("1.- Agregar Vehiculo");
		menu.addItemMenu("2.- Lista de Vehiculos");
		menu.addItemMenu("3.- Eliminar Vehiculo");
		menu.addItemMenu("4.- Cancelar Viaje");
		menu.addItemMenu("5.- Asignar viaje a Vehiculo");
		menu.addItemMenu("6.- Viajes asignados a Vehiculos");
		menu.addItemMenu("7.- Salir");
	}
	/*******************************************************************************/
	static String capturaRegistroMarca() {
		String registromarca;
		String mvregistromarca = FormInput.leeString("Ingrese la marca del vehiculo");
		registromarca = String.format(mvregistromarca);
		return registromarca;
	}

	static String capturamodelo() {
		String capturamodelo;
		String cmcapturamodelo = FormInput.leeString("Ingrese el modelo");
		capturamodelo = String.format(cmcapturamodelo);
		return capturamodelo;
	}

	static float capturacantidadMaxima() {
		float cantidadMaxima;
		float cmcantidadMaxima = FormInput.leeFloat("Ingrese la cantidad maxima de la carga");
		cantidadMaxima = Float.floatToIntBits(cmcantidadMaxima);
		return cantidadMaxima;
	}

	static LocalDate capturafechaAdquisicion() {
		LocalDate fechaAdquisicion;
		String facapturafechaAdquisicion = FormInput.leeString("Ingrese la fecha de adquisicion: [aaaa-mm-dd]:");
		fechaAdquisicion = LocalDate.parse(facapturafechaAdquisicion);
		return fechaAdquisicion;
	}

	static Vehiculo AgregarVehiculo() {
		String registroMarca = capturaRegistroMarca();
		String modelo = capturamodelo();
		float cantidadMaxima = capturacantidadMaxima();
		LocalDate fechaAdquisicion = capturafechaAdquisicion();
		return new Vehiculo(registroMarca, modelo, cantidadMaxima, fechaAdquisicion, viajes, 0);
	}

	static void addVehiculo() {
		Vehiculo vehiculo = AgregarVehiculo();
		vehiculos.add(vehiculo);
	}

	static void listaVehiculos() {
		if(!vehiculos.isEmpty())
			FormOutput.imprimeListaTabla(vehiculos, "Lista de Vehiculos");
	}

	static void EliminarVehiculo() {
		for (Vehiculo vehiculo : vehiculos)
			if (FormInput.leeBoolean(vehiculo + "Es el viaje a eliminar:")) {
			  vehiculos.remove(vehiculo);
				break;
			}
	}
	/*******************************************************************************/
	static String capturaciudadDestino() {
		String ciudadDestino;
		String cdciudadDestino = FormInput.leeString("Ingrese la ciudad de destino");
		ciudadDestino = String.format(cdciudadDestino);
		return ciudadDestino;
	}

	static String capturadireccion() {
		String direccion;
		String cddireccion = FormInput.leeString("Ingrese la direccion");
		direccion = String.format(cddireccion);
		return direccion;
	}

	static LocalDate capturafechaViaje() {
		LocalDate fechaViaje;
		String fvfechaViaje = FormInput.leeString("Ingrese la  fecha del viaje: [aaaa-mm-dd]:");
		fechaViaje = LocalDate.parse(fvfechaViaje);
		return fechaViaje;
	}

	static LocalDate capturafechaRegreso() {
		LocalDate fechaRegreso;
		String frfechaRegreso = FormInput.leeString("Ingrese la fecha de regreso: [aaaa-mm-dd]:");
		fechaRegreso = LocalDate.parse(frfechaRegreso);
		return fechaRegreso;
	}

	static String capturadescripcionCarga() {
		String descripcionCarga;
		String dcdescripcionCarga = FormInput.leeString("Ingrese la descripcion de la carga");
		descripcionCarga = String.format(dcdescripcionCarga);
		return descripcionCarga;
	}

	static String capturamontoViaje() {
		String montoViaje;
		String mvmontoViaje = FormInput.leeString("Ingrese el monto del viaje");
		montoViaje = String.format(mvmontoViaje);
		return montoViaje;
	}
	/**
	 * @throws ExcepcionValidaFechaRegreso*****************************************************************************/
	static Viajes Viajes() throws ExcepcionValidaFechaRegreso {
		String ciudadDestino = capturaciudadDestino();
		String direccion = capturadireccion();
		LocalDate FechaViaje = capturafechaViaje();
		LocalDate FechaRegreso = capturafechaRegreso();
		String descripcionCarga = capturadescripcionCarga();
		String montoViaje = capturamontoViaje();
		return new Viajes(ciudadDestino, direccion, FechaViaje, FechaRegreso, descripcionCarga, montoViaje);
	}
	/**
	 * @throws ExcepcionValidaFechaRegreso *****************************************************************************/
	@SuppressWarnings("unlikely-arg-type")
	static void addAsignarViaje() throws ExcepcionValidaFechaRegreso {
		Viajes viaje = Viajes();
		viajes.add(viaje);
		for (Viajes viajes : viajes)
			if (FormInput.leeBoolean(vehiculos + "Desea escoger este vehiculo para su viaje:")) {
				vehiculos.contains(viajes);
			}
	}

	static void CancelarViaje() {
		for (Viajes viaje : viajes)
			if (FormInput.leeBoolean(viaje + "Es el viaje a eliminar:")) {
				viajes.remove(viaje);
				break;
			}
	}
	
	static void ViajesAsignados() {
		FormOutput.imprimeListaTabla(viajes, "Viajes asignados");
		FormOutput.imprimeListaTabla(vehiculos, "Vehiculos");			
	}
	/**
	 * @throws ExcepcionValidaFechaRegreso *****************************************************************************/
	public static void run() throws ExcepcionValidaFechaRegreso {
		generaMenu();
		vehiculos = Persistencia.recuperaVehiculo();
		int opc;
		do {
			opc = menu.menuOption();
			switch (opc) {
			case 1:
				addVehiculo();
				break;
			case 2:
				listaVehiculos();
				break;
			case 3:
				EliminarVehiculo();
				break;
			case 4:
				CancelarViaje();
				break;
			case 5:
				addAsignarViaje();
				break;
			case 6:
				ViajesAsignados();
				break;
			}
		} while (opc != FIN);
		Persistencia.grabaVehiculos(vehiculos);
	}
}