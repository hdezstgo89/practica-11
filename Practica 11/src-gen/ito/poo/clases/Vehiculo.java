package ito.poo.clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.io.Serializable;

public class Vehiculo implements Comparable<Vehiculo>, Serializable {

	private static final long serialVersionUID = 1L;
	private String registroMarca;
	private String modelo;
	private float cantidadMaxima;
	private LocalDate fechaAdquisicion;
	private ArrayList<Viajes> viajesRealizados = new ArrayList<Viajes>();
	private int identificador;
	/*******************************************************************************/
	public Vehiculo() {
		super();
	}
	
	public Vehiculo(String registroMarca, String modelo, float cantidadMaxima, LocalDate fechaAdquisicion,
			ArrayList<Viajes> viajesRealizados, int identificador) {
		super();
		this.registroMarca = registroMarca;
		this.modelo = modelo;
		this.cantidadMaxima = cantidadMaxima;
		this.fechaAdquisicion = fechaAdquisicion;
		this.viajesRealizados = viajesRealizados;
		this.identificador = identificador;
	}
	/*******************************************************************************/
	public  boolean  addViaje (Viajes  newViaje) {
		boolean addViaje = false;
		addViaje = this.viajesRealizados.add(newViaje);
		return addViaje;
	}

	public  boolean  elimViaje ( Viajes  viaje ) {
		boolean elimViaje = false;
		elimViaje = this.viajesRealizados.remove(viaje);
		return elimViaje;
	}

	public String getRegistroMarca() {
		return registroMarca;
	}

	public void setRegistroMarca(String registroMarca) {
		this.registroMarca = registroMarca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public float getCantidadMaxima() {
		return cantidadMaxima;
	}

	public void setCantidadMaxima(float cantidadMaxima) {
		this.cantidadMaxima = cantidadMaxima;
	}

	public LocalDate getFechaAdquisicion() {
		return fechaAdquisicion;
	}

	public void setFechaAdquisicion(LocalDate fechaAdquisicion) {
		this.fechaAdquisicion = fechaAdquisicion;
	}

	public ArrayList<Viajes> getViajesRealizados() {
		return viajesRealizados;
	}

	public void setViajesRealizados(ArrayList<Viajes> viajesRealizados) {
		this.viajesRealizados = viajesRealizados;
	}

	public int getIdentificador() {
		return identificador;
	}

	public void setIdentificador(int identificador) {
		this.identificador = identificador;
	}
	/*******************************************************************************/
	@Override
	public String toString() {
		return "Vehiculo [registroMarca=" + registroMarca + ", modelo=" + modelo + ", cantidadMaxima=" + cantidadMaxima
				+ ", fechaAdquisici�n=" + fechaAdquisicion + ", viajesRealizados=" + viajesRealizados
				+ ", identificador=" + identificador + "]";
	}
	/*******************************************************************************/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(cantidadMaxima);
		result = prime * result + ((fechaAdquisicion == null) ? 0 : fechaAdquisicion.hashCode());
		result = prime * result + identificador;
		result = prime * result + ((modelo == null) ? 0 : modelo.hashCode());
		result = prime * result + ((registroMarca == null) ? 0 : registroMarca.hashCode());
		result = prime * result + ((viajesRealizados == null) ? 0 : viajesRealizados.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehiculo other = (Vehiculo) obj;
		if (Float.floatToIntBits(cantidadMaxima) != Float.floatToIntBits(other.cantidadMaxima))
			return false;
		if (fechaAdquisicion == null) {
			if (other.fechaAdquisicion != null)
				return false;
		} else if (!fechaAdquisicion.equals(other.fechaAdquisicion))
			return false;
		if (identificador != other.identificador)
			return false;
		if (modelo == null) {
			if (other.modelo != null)
				return false;
		} else if (!modelo.equals(other.modelo))
			return false;
		if (registroMarca == null) {
			if (other.registroMarca != null)
				return false;
		} else if (!registroMarca.equals(other.registroMarca))
			return false;
		if (viajesRealizados == null) {
			if (other.viajesRealizados != null)
				return false;
		} else if (!viajesRealizados.equals(other.viajesRealizados))
			return false;
		return true;
	}
	
	public int compareTo(Vehiculo arg0) {
		int r=0;
		if (!this.registroMarca.equals(arg0.getRegistroMarca()))
			return this.registroMarca.compareTo(arg0.getRegistroMarca());
		else if (!this.modelo.equals(arg0.getModelo()))
			return this.modelo.compareTo(arg0.getModelo());
		else if(this.cantidadMaxima != arg0.getCantidadMaxima())
			return this.cantidadMaxima>arg0.getCantidadMaxima()?1:-1;
		else if (!this.fechaAdquisicion.equals(arg0.getFechaAdquisicion()))
			return this.fechaAdquisicion.compareTo(arg0.getFechaAdquisicion());
		else if(this.identificador != arg0.getIdentificador())
			return this.identificador>arg0.getIdentificador()?1:-1;
		return r;
	}	
}
